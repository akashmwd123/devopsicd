package com.devopsicd.devopsicd

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET

interface TestResource {
    companion object {
        const val PATH = "/test"
    }

    @RequestMapping(
        method = [GET],
        value = ["/_get"],
    )
    fun getText(): String
}