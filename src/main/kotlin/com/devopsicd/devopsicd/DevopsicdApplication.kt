package com.devopsicd.devopsicd

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.ComponentScans

@SpringBootApplication
@ComponentScans
class DevopsicdApplication

fun main(args: Array<String>) {
	runApplication<DevopsicdApplication>(*args)

	println(" application started" )
}
