package com.devopsicd.devopsicd

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = [TestResource.PATH])
class TestController: TestResource {

    override fun getText(): String {
        return "akash"
    }
}